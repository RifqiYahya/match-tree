using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    #region Singleton
    static SoundManager _instance;
    public static SoundManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<SoundManager>();
                if (_instance == null) {
                    Debug.LogError("Fatal Error : Sound manager is not found");
                }
            }
            return _instance;
        }
    }
    #endregion

    public AudioClip ScoreNormal;
    public AudioClip ScoreCombo;

    public AudioClip WrongMove;

    public AudioClip Tap;

    AudioSource _player;

    // Start is called before the first frame update
    void Start()
    {
        _player = GetComponent<AudioSource>();
    }

    public void PlayScore(bool isCombo) {
        if(isCombo) {
            _player.PlayOneShot(ScoreCombo);
        } else {
            _player.PlayOneShot(ScoreNormal);
        }
    }

    public void PlayWrong() {
        _player.PlayOneShot(WrongMove);
    }

    public void PlayTap() {
        _player.PlayOneShot(Tap);
    }
}
