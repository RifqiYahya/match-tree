using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITime : MonoBehaviour
{
    public Text TimeRemain;

    string GettimeString(float timeRemaining) {
        int minute = Mathf.FloorToInt(timeRemaining/60);
        int second = Mathf.FloorToInt(timeRemaining%60);

        return $"{minute} : {second}";
    }

    public void Show() {
        gameObject.SetActive(true);
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        TimeRemain.text = GettimeString(TimeManager.Instance.GetRemainingTime() + 1);    
    }
}
