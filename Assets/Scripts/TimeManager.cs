using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    #region  Singleton
    static TimeManager _instance;
    public static TimeManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<TimeManager>();
                if(_instance == null) {
                    Debug.LogError("Fatal Error : Time manager is not found");
                }
            }
            return _instance;
        }
        
    }
    #endregion

    public int duration;
    float _time;

    // Start is called before the first frame update
    void Start()
    {
        _time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (FlowManager.Instance.IsGameOVer) {
            return;
        }

        if (_time > duration) {
            FlowManager.Instance.GameOver();
            return;
        }

        _time += Time.deltaTime;
    }

    public float GetRemainingTime() {
        return duration - _time;
    }
}
