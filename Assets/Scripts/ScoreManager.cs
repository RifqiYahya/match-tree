using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    static int _highScore;

    #region Singleton
    static ScoreManager _instance;
    public static ScoreManager Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<ScoreManager>();
                if(_instance == null) {
                    Debug.LogError("Fatal Error : Score manager is not found");
                }
            }

            return _instance;
        }
    }
    #endregion

    public int TileRatio;
    public int ComboRatio;

    int _currentScore;
    public int HighScore { get {return _highScore; }}
    public int CurrentScore { get {return _currentScore; }}

    public void ResetCurrentScore() {
        _currentScore = 0;
    }

    public void IncrementCurrentScore(int tileCount, int comboCount) {
        _currentScore += (tileCount*TileRatio) * (comboCount*ComboRatio);
        SoundManager.Instance.PlayScore(comboCount > 1);
    }

    public void SetHighScore() {
        if (_currentScore > _highScore) {
            _highScore = _currentScore;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetCurrentScore();
    }
}
