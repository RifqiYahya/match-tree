using UnityEngine;

public class FlowManager : MonoBehaviour
{
    #region Singleton
    static FlowManager _instance;
    public static FlowManager Instance {
        get {
            if(_instance == null) {
                _instance = FindObjectOfType<FlowManager>();
                if(_instance == null) {
                    Debug.LogError("Fatal Error : Flow manager is not found");
                }
            }
            return _instance;
        }
    }
    #endregion

    bool _isGameOver;
    public bool IsGameOVer { get {return _isGameOver; }}

    [Header("UI")]
    public UIGameOver GameOverUI;

    public void GameOver() {
        _isGameOver = true;
        ScoreManager.Instance.SetHighScore();
        GameOverUI.Show();
    }

    // Start is called before the first frame update
    void Start()
    {
        _isGameOver = false;
    }
}
