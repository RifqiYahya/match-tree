using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour
{
    public int Id;

    BoardManager _board;
    SpriteRenderer _renderer;

    static readonly Color _selectedColor = new Color(0.5f, 0.5f, 0.5f);
    static readonly Color _normalColor = Color.white;
    static TileController _previousSelected = null;
    bool _isSelected = false;

    static readonly float moveDuration = 0.5f;
    static readonly float destroyBigDuration = 0.1f;
    static readonly float destroySmallDuration = 0.4f;

    static readonly Vector2 sizeBig = Vector2.one*1.2f;
    static readonly Vector2 sizeSmall = Vector2.zero;
    static readonly Vector2 sizeNormal = Vector2.one;

    static readonly Vector2[] _adjacentDirection = new Vector2[]
        {Vector2.up, Vector2.down, Vector2.left, Vector2.right};
    
    public bool IsDestroyed { get; private set;}

    FlowManager _game; 

    public void GenerateRandomTile(int x, int y) {
        transform.localScale = sizeNormal;
        IsDestroyed = false;

        ChangeId(Random.Range(0, _board.TileTypes.Count), x, y);
    }

    public IEnumerator SetDestroyed (System.Action onCompleted) {
        IsDestroyed = true;
        Id = -1;
        
        name = "TILE_NULL";

        Vector2 startSize = transform.localScale;
        float time = 0.0f;

        while (time < destroyBigDuration) {
            transform.localScale = Vector2.Lerp(startSize, sizeBig, time/destroyBigDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = sizeBig;

        startSize = transform.localScale;
        time = 0.0f;

        while (time < destroySmallDuration) {
            transform.localScale = Vector2.Lerp(startSize, sizeSmall, time/destroySmallDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = sizeSmall;

        _renderer.sprite = null;

        onCompleted?.Invoke();
    }

    #region Check Match
    List<TileController> GetMatch(Vector2 castDir) {
        List<TileController> matchingTiles = new List<TileController>();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir, _renderer.size.x);

        while (hit) {
            TileController otherTile = hit.collider.GetComponent<TileController>();
            if (otherTile.Id != Id || otherTile.IsDestroyed) {
                break;
            }

            matchingTiles.Add(otherTile);
            hit = Physics2D.Raycast(otherTile.transform.position, castDir, _renderer.size.x);
        }

        return matchingTiles;
    }

    List<TileController> GetOneLineMatch(Vector2[] paths) {
        List<TileController> matchingTiles = new List<TileController>();

        for (int i=0; i < paths.Length; i++) {
            matchingTiles.AddRange(GetMatch(paths[i]));
        }

        if (matchingTiles.Count >= 2) {
            return matchingTiles;
        }

        return null;
    }

    public List<TileController> GetAllMatches() {
        if (IsDestroyed) {
            return null;
        }

        List<TileController> matchingTiles = new List<TileController> ();

        //Get matches for horizontal and vertical
        List<TileController> horizontalMatchingTiles =
            GetOneLineMatch(new Vector2[2] {Vector2.up, Vector2.down});
        
        List<TileController> verticalMatchingTiles =
            GetOneLineMatch(new Vector2[2] {Vector2.left, Vector2.right});
        
        if (horizontalMatchingTiles != null) {
            matchingTiles.AddRange(horizontalMatchingTiles);
        }

        if (verticalMatchingTiles != null) {
            matchingTiles.AddRange(verticalMatchingTiles);
        }

        //add itself to the matching tiles
        if (matchingTiles != null && matchingTiles.Count >= 2) {
            matchingTiles.Add(this);
        }

        return matchingTiles;
    }
    #endregion

    #region Adjacent
    TileController GetAdjacent(Vector2 castDir) {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir, _renderer.size.x);

        if(hit) {
            return hit.collider.GetComponent<TileController>();
        }

        return null;
    }

    public List<TileController> GetAllAdjacentTiles() {
        List<TileController> adjacentTiles = new List<TileController>();

        foreach(Vector2 adjacentDirection in _adjacentDirection) {
            adjacentTiles.Add(GetAdjacent(adjacentDirection));
        }

        return adjacentTiles;
    }
    #endregion

    public IEnumerator MoveTilePosition(Vector2 targetPosition, System.Action onCompleted) {
        Vector2 startPosition = transform.position;
        float time = 0.0f;

        //run anumation on next frame for safety reason
        yield return new WaitForEndOfFrame();

        while(time < moveDuration) {
            transform.position = Vector2.Lerp(startPosition, targetPosition, time/moveDuration);
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = targetPosition;
        onCompleted?.Invoke();
    }

    #region Select & Deselect
    void Select() {
        _isSelected = true;
        _renderer.color = _selectedColor;
        _previousSelected = this;
    }

    void Deselect() {
        _isSelected = false;
        _renderer.color = _normalColor;
        _previousSelected = null;
    }
    #endregion

    public void SwapTile(TileController otherTile, System.Action onCompleted = null) {
        StartCoroutine(_board.SwapTilePosition(this, otherTile, onCompleted));
    }

    void OnMouseDown() {
        //Non Selectable conditions
        if (_renderer.sprite == null || _board.IsAnimating || _game.IsGameOVer) {
            return;
        }

        SoundManager.Instance.PlayTap();

        //Already selected
        if(_isSelected) {
            Deselect();
        } else {
            //if nothing selected yet
            if(_previousSelected == null) {
                Select();
            } else {
                if(GetAllAdjacentTiles().Contains(_previousSelected)) {
                    TileController otherTile = _previousSelected;
                    _previousSelected.Deselect();

                    //swap tile
                    SwapTile(otherTile, () => {
                        if(_board.GetAllMatches().Count > 0) {
                            // Debug.Log("MATCH FOUND");
                            _board.Process();
                        } else {
                            SoundManager.Instance.PlayWrong();
                            SwapTile(otherTile);
                        }
                    });
                } else {
                    //run if cant swap
                    _previousSelected.Deselect();
                    Select();
                }
            }
        }
    }

    public void ChangeId(int id, int x, int y) {
        _renderer.sprite = _board.TileTypes[id];
        this.Id = id;

        name = "TILE_" + id + "("+ x + "," + y +")";
    }

    void Awake() {
        _board = BoardManager.Instance;
        _renderer = GetComponent<SpriteRenderer>();
        _game = FlowManager.Instance;
    }

    void Start() {
        IsDestroyed = false;
    }
}
